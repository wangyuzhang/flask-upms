# -*- coding: utf-8 -*-
"""
    数据模型
    Created by Quant on 2018-03-07
"""
__author__ = 'Quant'

from flask_sqlalchemy import SQLAlchemy, Model

# 初始化SQLAlchemy
# 将会从config.py文件中加载SQLALCHEMY_DATABASE_URL的配置
# SQLAlchemy 会自动的从 app 对象中的 DevConfig、ProdConfig、TestConfig 中加载连接数据库的配置项
db = SQLAlchemy()


# 用户管理
class User(db.Model):
    __tablename__ = 't_user'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    email = db.Column(db.String(50), unique=True)
    username = db.Column("user_name", db.String(50), unique=True)
    password = db.Column(db.String(100))
    avatar = db.Column(db.String(100))
    login_time = db.Column(db.Integer)


    def __repr__(self):
        return "<Model User `{}`>".format(self.username)