# -*- coding: utf-8 -*-
"""
 用户管理
 Created by Quant on 2018-03-07
"""
__author__ = 'Quant'
from flask import Flask
from app.models import db
from app.users import users as users_blueprint
from app.auth import auth as auth_blueprint
from app.core.framework import auth_filter
from app.core.redis import redis


def create_app(object_name):
    app = Flask(__name__)

    # 设置App的配置
    app.config.from_object(object_name)

    # 钩子函数，在每次执行请求之前运行
    app.before_request_funcs.setdefault(None, []).append(auth_filter)
    # 初始化app
    db.init_app(app)

    redis.init_app(app)

    # 注册蓝图
    app.register_blueprint(users_blueprint)
    app.register_blueprint(auth_blueprint)

    return app
