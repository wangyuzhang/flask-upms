# -*- coding: utf-8 -*-
"""
 用户管理
 Created by Quant on 2018-03-07
"""
__author__ = 'Quant'

from flask import Blueprint
from flask_restful import Api

# 导入路由
from app.users.api import Users, UserList,UserRegister

# users  蓝图
users = Blueprint('users', __name__, url_prefix="/users")

# 构建flask-restfull的Api对象
user_api = Api(users)

# 构建路由
user_api.add_resource(Users, '/<int:id>')
user_api.add_resource(UserList, '/')
user_api.add_resource(UserRegister, '/register')
