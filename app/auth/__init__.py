# -*- coding: utf-8 -*-
"""
 用户管理
 Created by Quant on 2018-03-07
"""
__author__ = 'Quant'

from flask import Blueprint
from flask_restful import Api

# 导入路由
from app.auth.api import Login

# users  蓝图
auth = Blueprint('auth', __name__, url_prefix="/auth")

# 构建flask-restfull的Api对象
auth_api = Api(auth)

auth_api.add_resource(Login, '/token')
