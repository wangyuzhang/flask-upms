# -*- coding: utf-8 -*-
"""
 多环境配置
 Created by Quant on 2018-03-07
"""
__author__ = 'Quant'


# 公共基础配置
class BaseConfig(object):
    """Base config class."""
    DEBUG = True
    PORT = 8008
    HOST = '0.0.0.0'
    SECRET_KEY = '123456789qwertyuiopasdfghjklzxcvbnm'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    #设置拦截器忽略的条件
    AUTH_IGNORE_URL = ['/auth/token', '/users/register']

    # redis的基本配置
    REDIS_URL = 'redis://localhost:6379/0'


# 生产环境配置
class ProdConfig():
    """Production config class."""
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:root@127.0.0.1:3306/gateway-cloud?charset=utf8"


# 测试环境环境配置
class TestConfig(BaseConfig):
    """Development config class."""
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:root@127.0.0.1:3306/gateway-cloud?charset=utf8"


# 开发环境环境配置
class DevConfig(BaseConfig):
    """Development config class."""
    # Open the DEBUG
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:root@127.0.0.1:3306/gateway-cloud?charset=utf8"
