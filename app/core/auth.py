# -*- coding: utf-8 -*-
"""
 授权管理
 Created by Quant on 2018-03-07
"""
__author__ = 'Quant'

import jwt, datetime
from flask import current_app
from app.models import User
from app.core import util
from app.core.code import Code


# 生成认证Token
def create_auth_token(user_id):
    """
    生成认证Token
    :param user_id: int
    :return: string
    """
    try:
        payload = {
            'iat': datetime.datetime.utcnow(),  # 在什么时候签发的(UNIX时间)，是否使用是可选的；
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=10),  # 什么时候过期，这里是一个Unix时间戳，是否使用是可选的；
            'iss': '',  # 该JWT的签发者，是否使用是可选的；
            'sub': user_id,  # 该JWT所面向的用户，是否使用是可选的
            'user_id': user_id
        }
        return jwt.encode(payload, current_app.config['SECRET_KEY'], algorithm='HS256')

    except Exception as e:
        return e


# 转换payload对象
def parse_auth_token(auth_token):
    """
    转换payload对象
    :param auth_token:
    :return: payload 对象
    """
    try:
        # payload = jwt.decode(auth_token, app.config.get('SECRET_KEY'), leeway=datetime.timedelta(seconds=10))
        # 取消过期时间验证
        payload = jwt.decode(auth_token, current_app.config['SECRET_KEY'], options={'verify_exp': False})
        if ('user_id' in payload):
            return payload
        else:
            raise jwt.InvalidTokenError
    except jwt.ExpiredSignatureError:
        return util.error_result(Code.INVALIDATE_TOKEN, 'Token过期')
    except jwt.InvalidTokenError:
        return util.error_result(Code.INVALIDATE_TOKEN, '无效Token')


# 检查token的合法性
def check_auth_token(auth_token):
    """
    检查token的合法性
    :param auth_token:
    :return:对象
    """
    if (not auth_token):
        util.error_result("没有提供认证token参数")

    # 转换的payload对象
    payload = parse_auth_token(auth_token)
    # 确保payload不是字符串类型
    if not isinstance(payload, str):
        if ("user_id" in payload and payload['user_id'] is not None):
            user = User.query.filter_by(id=payload['user_id']).first()
            if (user is None):
                result = util.error_result('找不到该用户信息')
            else:
                result = util.success_result(user, '验证成功')
        else:
            result = util.error_result(payload, '找不到用户信息')
    else:
        result = util.error_result(payload, 'payload类型错误')
    return result
