# -*- coding: utf-8 -*-
"""
 自定义状态码
 Created by Quant on 2018-03-08
"""
__author__ = 'Quant'


class Code:
    SUCCESS = 200
    UNAUTHORIZED = 401
    NOT_FOUND = 404
    INTERNAL_SERVER_ERROR = 500
    # token错误
    INVALIDATE_TOKEN = 9999
