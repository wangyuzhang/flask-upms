# -*- coding: utf-8 -*-
"""
 Created by Quant on 2018-03-16
"""
__author__ = 'Quant'


class Const:
    # 认证授权常量
    AUTH_HEADER = 'Authorization'
    AUTH_HEADER_PREFIX = 'JWT'

    # 在线用户列表常量
    AUTH_TOKEN_PREFIX = 'jwt:token:'
    AUTH_TOKEN = AUTH_TOKEN_PREFIX + '{0}'
    # 默认token过期时间30分钟 30*60 = 1800秒
    AUTH_TOKEN_EXPIRES = 1800
