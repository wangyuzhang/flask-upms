# -*- coding: utf-8 -*-
"""
 日志处理模块
 Created by Quant on 2018-03-12
"""
__author__ = 'Quant'
import logging, sys


def log():
    """
    定义日志的公共配置
    :return:
    """
    # 获取logger实例，如果参数为空则返回root
    logger = logging.getLogger('')

    # 指定logger输出格式
    fmt = logging.Formatter('%(asctime)s %(levelname)-8s: %(message)s')

    # 文件日志
    fh = logging.FileHandler('server-log.txt', encoding='utf-8')
    fh.setFormatter(fmt)  # 可以通过setFormatter指定输出格式

    # 控制台日志
    sh = logging.StreamHandler()
    sh.setFormatter(fmt)

    # 为logger添加的日志处理器
    logger.addHandler(fh)
    logger.addHandler(sh)

    # 指定日志的最低输出级别，默认为WARN级别
    logger.setLevel(logging.INFO)

    return logger


log = log()
