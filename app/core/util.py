# -*- coding: utf-8 -*-
"""
 常用工具类
 Created by Quant on 2018-03-08
"""
__author__ = 'Quant'

from flask_restful import abort
from app.core.code import Code
from app.core.logging import log


# 成功返回
def success_result(msg=None):
    return {"code": Code.SUCCESS, "success": True, "data": None, "msg": msg}


# 成功返回
def success_result(data=None, msg=None):
    return {"code": Code.SUCCESS, "success": True, "data": data, "msg": msg}


# 错误返回
def error_result(code=Code.SUCCESS, data=None, msg=None):
    return {"code": code, "success": False, "data": data, "msg": msg}


# 错误返回
def error_result(msg=None):
    return {"code": Code.INTERNAL_SERVER_ERROR, "success": False, "data": None, "msg": msg}


def error_result(data=None, msg=None):
    return {"code": Code.INTERNAL_SERVER_ERROR, "success": False, "data": data, "msg": msg}


def error_result(code=Code.INTERNAL_SERVER_ERROR, data=None, msg=None):
    return {"code": code, "success": False, "data": data, "msg": msg}


# 自定义构建返回500错误的参数
def make_abort_500(msg=None):
    log.error(msg)  # 日志输出
    abort(http_status_code=Code.INTERNAL_SERVER_ERROR, code=Code.INTERNAL_SERVER_ERROR, success=False, data=None,
          msg=msg)


# 自定义构建返回错误的参数
def make_abort(code, msg=None):
    log.error(msg)  # 日志输出
    abort(http_status_code=code, code=code, success=False, data=None, msg=msg)


# 自定义构建返回错误的参数
def make_abort(code, data=None, msg=None):
    log.error(msg)  # 日志输出
    abort(http_status_code=code, code=code, success=False, data=data, msg=msg)
