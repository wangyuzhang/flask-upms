# -*- coding: utf-8 -*-
"""
 Created by Quant on 2018-03-07
"""
__author__ = 'Quant'

import os
from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand

from app import create_app
from app import models

# 获取环境变量中配置的CLOUD_ENV,不设置（Prod、Test、Dev），默认就是dev
env = os.environ.get('CLOUD_ENV', 'dev')

# 工厂方法创建app
app = create_app('app.config.{}Config'.format(env.capitalize()))

# 创建命令行解释器
manager = Manager(app)

# 初始化migrate
migrate = Migrate(app, models.db)

# 创建命令行可执行的命令
manager.add_command("server", Server(host=app.config['HOST'],
                                     port=app.config['PORT'],
                                     use_debugger=app.config['DEBUG']))
manager.add_command("db", MigrateCommand)


@manager.shell
def make_shell_context():
    return dict(app=app,
                db=models.db,
                User=models.User
                )


if __name__ == '__main__':
    manager.run(default_command="server")
